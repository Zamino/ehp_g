//CUDA Kernels fuer Verwendung von Integer in Aufgabe 3.2

//TODO: weitere Makros definieren
#define getR(img) (((unsigned int)img&0xFF000000)>>24)
#define getG(img) (((unsigned int)img&0x00FF0000)>>16)
#define getB(img) (((unsigned int)img&0x0000FF00)>>8)
#define getA(img) (((unsigned int)img&0x000000FF))
#define output(r,g,b,a) (((unsigned int)r<<24)+((unsigned int)g<<16)\
                        +((unsigned int)b<<8) +((unsigned int)a<<0))

__global__ void copyImgKernel(unsigned int* img_in, unsigned int* img_out, int width, int height)
{
   //TODO: Copy Kernel implementieren
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adr=(i+j*width);
      unsigned int r,g,b,a;
      r=getR(img_in[adr]);
      g=getG(img_in[adr]);
      b=getB(img_in[adr]);
      a=getA(img_in[adr]);

      img_out[adr]=output(r,g,b,a);
   }
}

__global__ void linearTransformKernel(unsigned int* img_in, unsigned int* img_out, int width, int height, float alpha, float beta)
{
   //TODO: Helligkeit und Kontrast
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adr=(i+j*width);
      float r,g,b;
      unsigned int a;
      r=(float) getR(img_in[adr]);
      g=(float) getG(img_in[adr]);
      b=(float) getB(img_in[adr]);
      a=getA(img_in[adr]);

      r = alpha * r + beta;
      g = alpha * g + beta;
      b = alpha * b + beta;

      if(r < 0) {
        r = 0;
      } else if (r > 255) {
        r = 255;
      }
      if(g < 0) {
        g = 0;
      } else if (g > 255) {
        g = 255;
      }
      if(b < 0) {
        b = 0;
      } else if (b > 255) {
        b = 255;
      }

      img_out[adr]=output(r,g,b,a);
   }
}

__global__ void mirrorKernel(unsigned int* img_in, unsigned int* img_out, int width, int height)
{
   //TODO: Spiegeln
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adr=(i+j*width);
      unsigned int r,g,b,a;
      if ( i > width/2) {
        int adrNew=(width-i+j*width);
        r=getR(img_in[adrNew]);
        g=getG(img_in[adrNew]);
        b=getB(img_in[adrNew]);
        a=getA(img_in[adrNew]);
      } else {
        r=getR(img_in[adr]);
        g=getG(img_in[adr]);
        b=getB(img_in[adr]);
        a=getA(img_in[adr]);
      }

      img_out[adr]=output(r,g,b,a);
   }
}

__global__ void bwKernel(unsigned int* img_in, unsigned int* img_out, int width, int height)
{
   //TODO: Graubild erstellen
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adr=(i+j*width);
      unsigned int r,g,b,a;
      r=getR(img_in[adr]);
      g=getG(img_in[adr]);
      b=getB(img_in[adr]);
      a=getA(img_in[adr]);

      unsigned char bw = (r+g+b)/3;

      img_out[adr]=output(bw,bw,bw,a);
   }
}

__global__ void sobelKernel(unsigned int* img_in, unsigned int* img_out, int width, int height)
{

   //TODO: Kantendetektion mit Sobelfilter Kernel implementieren
   //Kommentieren Sie die folgenden Anweisungen aus um die SX und SY Arrays zu erhalten
   const float SX[3][3]={{-1,0,1},{-2,0,2},{-1,0,1}};
   const float SY[3][3]={{-1,-2,-1},{0,0,0},{1,2,1}};

   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;
   int adr=(i+j*width);

   unsigned char bw = 0;

   if (i>0 && i<width-1 && j>0 && j<height-1) {
     float g_x = 0;
     float g_y = 0;

     for(int k=-1; k<2; k++) {
       for(int l=-1; l<2; l++) {
         int adr_tmp=(i+k+(j+l)*width);
         g_x = g_x + SX[k+1][l+1] * getR(img_in[adr_tmp]);
         g_y = g_y + SY[k+1][l+1] * getR(img_in[adr_tmp]);
       }
     }
     bw = (unsigned char) sqrt(g_x*g_x + g_y*g_y);
     if(bw > 255) {
       bw = 255;
     }
   }

   if (i<width && j<height) {
     unsigned int a;
     a=getA(img_in[adr]);

     img_out[adr]=output(bw,bw,bw,a);
   }

}
