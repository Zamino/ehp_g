all:
	nvcc -arch=sm_35 -o cuda-prak main.cu -lpng

nvvp:
	nvcc -arch=sm_35 -o cuda-nvvp perf.cu -lpng 

nvvp-int:
	nvcc -arch=sm_35 -o cuda-nvvp-int perf.cu -lpng -DUSEINT

int:
	nvcc -arch=sm_35 -o cuda-prak-int main.cu -lpng -DUSEINT

test: test.cu
	nvcc -arch=sm_35 -o test test.cu
