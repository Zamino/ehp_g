# Read file in, save each line in a seperate string each strin in a list
import matplotlib
import re
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

def autolabel(rects):
    #Attach a text label above each bar in *rects*, displaying its height.
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 3, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        
#dateien = ['time_analyse.txt',
#           'time_analyse_img_in_S.txt',
#           'time_analyse_img_in_M.txt',
#           'time_analyse_img_in_L.txt']

#for curFile in dateien:
dokument = []
strippedDokument = []

with open('time_analyse.txt','rt') as inputFile:
    dokument = inputFile.readlines()
for i in range(6):
    del dokument[0];
for i in range(6):
    tmp = []
    tmp.append(dokument[0])
    tmp.append(dokument[2].split(": "))
    tmp.append(dokument[3].split(": "))
    tmp.append(dokument[4].split(": "))
    tmp.append(dokument[5].split(": "))
    tmp.append(dokument[6].split(": "))
    strippedDokument.append(tmp.copy())
    for j in range(8):
        del dokument[0]
                
for i in range(len(strippedDokument)):
    for j in range(1,6):
        strippedDokument[i][j][1] = float(strippedDokument[i][j][1].replace(" ms\n",""))

time_analyse_Data = []
time_analyse_Data = strippedDokument.copy()
inputFile.close()

dokument = []
strippedDokument = []
with open('time_analyse_img_in_S.txt','rt') as inputFile:
    dokument = inputFile.readlines()

for i in range(6):
    del dokument[0];

for i in range(6):
    tmp = []
    tmp.append(dokument[0])
    tmp.append(dokument[2].split(": "))
    tmp.append(dokument[3].split(": "))
    tmp.append(dokument[4].split(": "))
    tmp.append(dokument[5].split(": "))
    tmp.append(dokument[6].split(": "))
    strippedDokument.append(tmp.copy())
    for j in range(8):
        del dokument[0]
        
for i in range(len(strippedDokument)):
    for j in range(1,6):
        strippedDokument[i][j][1] = float(strippedDokument[i][j][1].replace(" ms\n",""))
        
time_analyse_S_Data = []
time_analyse_S_Data = strippedDokument.copy()
inputFile.close()

dokument = []
strippedDokument = []
with open('time_analyse_img_in_M.txt','rt') as inputFile:
    dokument = inputFile.readlines()

for i in range(6):
    del dokument[0];

for i in range(6):
    tmp = []
    tmp.append(dokument[0])
    tmp.append(dokument[2].split(": "))
    tmp.append(dokument[3].split(": "))
    tmp.append(dokument[4].split(": "))
    tmp.append(dokument[5].split(": "))
    tmp.append(dokument[6].split(": "))
    strippedDokument.append(tmp.copy())
    for j in range(8):
        del dokument[0]
        
for i in range(len(strippedDokument)):
    for j in range(1,6):
        strippedDokument[i][j][1] = float(strippedDokument[i][j][1].replace(" ms\n",""))

time_analyse_M_Data = []
time_analyse_M_Data = strippedDokument.copy()
inputFile.close()        

dokument = []
strippedDokument = []
with open('time_analyse_img_in_L.txt','rt') as inputFile:
    dokument = inputFile.readlines()

for i in range(6):
    del dokument[0];

for i in range(6):
    tmp = []
    tmp.append(dokument[0])
    tmp.append(dokument[2].split(": "))
    tmp.append(dokument[3].split(": "))
    tmp.append(dokument[4].split(": "))
    tmp.append(dokument[5].split(": "))
    tmp.append(dokument[6].split(": "))
    strippedDokument.append(tmp.copy())
    for j in range(8):
        del dokument[0]
        
for i in range(len(strippedDokument)):
    for j in range(1,6):
        strippedDokument[i][j][1] = float(strippedDokument[i][j][1].replace(" ms\n",""))

time_analyse_L_Data = []
time_analyse_L_Data = strippedDokument.copy()
inputFile.close()

# vergleich der Kernel bei unterschiedlichen Bildgrößen

for j in range(1,6): # Durchlauf der Kernel
    zeiten_S = []
    zeiten_M = []
    zeiten_L = []
    print(time_analyse_Data[0][j][0])
     # Zeiten der einzelen Konfigurationen nacheinander ins array legen
    for i in range(len(time_analyse_Data)):
        zeiten_S.append(round(time_analyse_S_Data[i][j][1],2))
        zeiten_M.append(round(time_analyse_M_Data[i][j][1],2))
        zeiten_L.append(round(time_analyse_L_Data[i][j][1],2))

    
    xBezeichner = ["1x1", "2x2", "4x4", "8x8", "16x16", "32x32"]
    width = 0.3 # the width of the bars
    index = np.arange(len(xBezeichner))
    # lineare y-Achse
    fig, ax = plt.subplots()
    rects_S = ax.bar(index - width*(2/3), zeiten_S, width, color="red", label='S')
    rects_M = ax.bar(index - width*(1/3), zeiten_M, width, color="green", label='M')
    rects_L = ax.bar(index + width*(2/3), zeiten_L, width, color="blue", label='L')
    ax.set_title(time_analyse_Data[i][j][0])
    ax.set_xlabel("Thread-Block-Configuration")
    ax.set_ylabel("Millisekunden")
    ax.set_xticks(index)
    ax.set_xticklabels(xBezeichner)

    #autolabel(rects_S)
    #autolabel(rects_M)
    #autolabel(rects_L)

    fig.tight_layout()
    plt.legend(loc='best')
    #plt.show()
    saveName = time_analyse_Data[i][j][0] + "_lin.png"
    fig.savefig(saveName)

    # logarithmische y-Achse
    fig, ax = plt.subplots()
    ax.set_yscale('log',subs = [1,2,3,4,5,6,7,8,9,10]) # logarithmische y-Achse
    rects_S = ax.bar(index-width/3, zeiten_S, width, color="red", label='S')
    rects_M = ax.bar(index, zeiten_M, width, color="green", label='M')
    rects_L = ax.bar(index + width/3, zeiten_L, width, color="blue", label='L')
    ax.set_title(time_analyse_Data[i][j][0])
    ax.set_xlabel("Thread-Block-Configuration")
    ax.set_ylabel("Millisekunden")
    ax.set_xticks(index)
    ax.set_xticklabels(xBezeichner)

    autolabel(rects_S)
    autolabel(rects_M)
    autolabel(rects_L)

    fig.tight_layout()
    plt.legend(loc='best')
    #plt.show()
    saveName = time_analyse_Data[i][j][0] + "_log.png"
    fig.savefig(saveName)

# bildgröße M übersicht über alle kernel
for j in range(1,6):
    zeit = []
    for i in range(len(time_analyse_M_Data)):

        zeit.append(round(time_analyse_M_Data[i][j][1],2))

    xBezeichner = ["1x1", "2x2", "4x4", "8x8", "16x16", "32x32"]
    index = np.arange(len(xBezeichner))
    fig, ax = plt.subplots()
    #ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f')) # y-Achse runden
    ax.set_yscale('log',subs = [1,2,3,4,5,6,7,8,9,10]) # logarithmische y-Achse
    rects = ax.bar(index, zeit, 0.9, color="green")
    ax.set_title(strippedDokument[i][j][0])
    ax.set_xlabel("Thread-Block-Configuration")
    ax.set_ylabel("Millisekunden")
    ax.set_xticks(index)
    ax.set_xticklabels(xBezeichner)

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x(), height),
                    xytext=(30,1), # vertikal offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        
    #plt.show()
    fig.savefig('kernels_M_log.png')

for j in range(1,6):
    zeit = []
    for i in range(len(time_analyse_M_Data)):

        zeit.append(round(time_analyse_M_Data[i][j][1],2))

    xBezeichner = ["1x1", "2x2", "4x4", "8x8", "16x16", "32x32"]
    index = np.arange(len(xBezeichner))
    fig, ax = plt.subplots()
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f')) # y-Achse runden
    #ax.set_yscale('log',subs = [1,2,3,4,5,6,7,8,9,10]) # logarithmische y-Achse
    rects = ax.bar(index, zeit, 0.9, color="green")
    ax.set_title(strippedDokument[i][j][0])
    ax.set_xlabel("Thread-Block-Configuration")
    ax.set_ylabel("Millisekunden")
    ax.set_xticks(index)
    ax.set_xticklabels(xBezeichner)

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x(), height),
                    xytext=(30,1), # vertikal offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        
    #plt.show()
    fig.savefig('kernels_M_lin.png')
    
        




        
        
