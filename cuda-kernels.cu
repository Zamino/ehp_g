__global__ void copyImgKernel(unsigned char* img_in, unsigned char* img_out, int width, int height)
{
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adrIn=(i+j*width)*4;
      int adrOut=adrIn;
      unsigned char r,g,b,a;
      r = img_in[adrIn+0];
      g = img_in[adrIn+1];
      b = img_in[adrIn+2];
      a = img_in[adrIn+3];

      img_out[adrOut+0] = r; 
      img_out[adrOut+1] = g;
      img_out[adrOut+2] = b;
      img_out[adrOut+3] = a;
   }
}

__global__ void linearTransformKernel(unsigned char* img_in, unsigned char* img_out, int width, int height, float alpha, float beta)
{
   //TODO: Aufgabe 2.2 Helligkeit und Kontrast
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adrIn=(i+j*width)*4;
      int adrOut=adrIn;
      float r,g,b;
      unsigned char a;
      r = (float) img_in[adrIn+0];
      g = (float) img_in[adrIn+1];
      b = (float) img_in[adrIn+2];
      a = img_in[adrIn+3];

      r = alpha * r + beta;
      g = alpha * g + beta;
      b = alpha * b + beta;

      if(r < 0) {
        r = 0;
      } else if (r > 255) {
        r = 255;
      }
      if(g < 0) {
        g = 0;
      } else if (g > 255) {
        g = 255;
      }
      if(b < 0) {
        b = 0;
      } else if (b > 255) {
        b = 255;
      }

      img_out[adrOut+0] = (unsigned char) r; 
      img_out[adrOut+1] = (unsigned char) g;
      img_out[adrOut+2] = (unsigned char) b;
      img_out[adrOut+3] = a;
   }
}

__global__ void mirrorKernel(unsigned char* img_in, unsigned char* img_out, int width, int height)
{
   //TODO: Aufgabe 2.3 Spiegeln
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adrIn=(i+j*width)*4;
      int adrOut=adrIn;
      unsigned char r,g,b,a;
      if ( i > width/2) {
        int adrNew=(width-i+j*width)*4;
        r = img_in[adrNew+0];
        g = img_in[adrNew+1];
        b = img_in[adrNew+2];
        a = img_in[adrNew+3];
      } else {
        r = img_in[adrIn+0];
        g = img_in[adrIn+1];
        b = img_in[adrIn+2];
        a = img_in[adrIn+3];
      }

      img_out[adrOut+0] = r;
      img_out[adrOut+1] = g;
      img_out[adrOut+2] = b;
      img_out[adrOut+3] = a;
   }
}

__global__ void bwKernel(unsigned char* img_in, unsigned char* img_out, int width, int height)
{
   //TODO: Aufgabe 2.4 Graubild erstellen
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;

   if (i<width && j<height)
   {
      int adrIn=(i+j*width)*4;
      int adrOut=adrIn;
      unsigned char r,g,b,a;
      r = img_in[adrIn+0];
      g = img_in[adrIn+1];
      b = img_in[adrIn+2];
      a = img_in[adrIn+3];

      unsigned char bw = (r+g+b)/3;

      img_out[adrOut+0] = bw;
      img_out[adrOut+1] = bw;
      img_out[adrOut+2] = bw;
      img_out[adrOut+3] = a;
   }

}

__global__ void sobelKernel(unsigned char* img_in, unsigned char* img_out, int width, int height)
{

   //TODO: Aufgabe 2.5 Kantendetektion mit Sobelfilter Kernel implementieren
   //Kommentieren Sie die folgenden Anweisungen aus um die SX und SY Arrays zu erhalten
   const float SX[3][3]={{-1,0,1},{-2,0,2},{-1,0,1}};
   const float SY[3][3]={{-1,-2,-1},{0,0,0},{1,2,1}};

   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int j = threadIdx.y+blockIdx.y*blockDim.y;
   int adrIn=(i+j*width)*4;
   int adrOut=adrIn;

   unsigned char bw = 0;

   if (i>0 && i<width-1 && j>0 && j<height-1) {
     float g_x = 0;
     float g_y = 0;

     for(int k=-1; k<2; k++) {
       for(int l=-1; l<2; l++) {
         int adr_tmp=(i+k+(j+l)*width)*4;
         g_x = g_x + SX[k+1][l+1] * img_in[adr_tmp];
         g_y = g_y + SY[k+1][l+1] * img_in[adr_tmp];
       }
     }
     bw = (unsigned char) sqrt(g_x*g_x + g_y*g_y);
     if(bw > 255) {
       bw = 255;
     }
   }

   if (i<width && j<height)
   {
     unsigned char a;
     a = img_in[adrIn+3];

     img_out[adrOut+0] = bw;
     img_out[adrOut+1] = bw;
     img_out[adrOut+2] = bw;
     img_out[adrOut+3] = a;
   }
}
